﻿using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWithLinq.Forms
{

    public partial class frm_Product : frm_Master
    {
        DAL.Product product;
        RepositoryItemLookUpEdit repoUOM = new RepositoryItemLookUpEdit();
        DAL.dbDataContext sdb = new DAL.dbDataContext();
        public frm_Product()
        {
            InitializeComponent();
            RefreshData();
            New();
        }
        public override void New()
        {
            product = new DAL.Product();
            base.New();
            var data = gridView1.DataSource as BindingList<DAL.ProductUnit>;
            var db = new DAL.dbDataContext();
            if(db.UnitNames.Count () == 0)
            {
                db.UnitNames.InsertOnSubmit(new DAL.UnitName() { Name = "قطعه" });
                db.SubmitChanges();
                RefreshData();
            }
            data.Add(new DAL.ProductUnit() { Factor = 1, UnitID = db.UnitNames.First().ID });
        }


        public override void GetData()
        {
            txt_Code.Text = product.Code;
            txt_Name.Text = product.Name;
            lkp_Category.EditValue = product.CategoryID;
            lkp_Type.EditValue = product.Type;
            memoEdit1.Text = product.Descreption;
            checkEdit1.Checked = product.IsActive;

           
            gridControl1.DataSource = sdb.ProductUnits.Where(x => x.ProductID == product.ID);
            base.GetData();
        }
        public override void SetData()
        {
            product.CategoryID = Convert.ToInt32(lkp_Category.EditValue);
            product.Code = txt_Code.Text;
            product.Name = txt_Name.Text;
            product.Type = Convert.ToByte(lkp_Type.EditValue);
            product.IsActive = checkEdit1.Checked;
            product.Image =GetByteFromImage( pictureEdit1.Image);
            base.SetData();
        }

        bool ValdiateData()
        {
            if (lkp_Category.EditValue is int == false)
            {
                lkp_Category.ErrorText = ErrorText;
                return false;
            }
            if (lkp_Type.EditValue is byte == false)
            {
                lkp_Type.ErrorText = ErrorText;
                return false;
            }
            if (txt_Name.Text.Trim() == string.Empty)
            {
                txt_Name.ErrorText = ErrorText;
                return false;
            }
            if (txt_Code.Text.Trim() == string.Empty)
            {
                txt_Code.ErrorText = ErrorText;
                return false;
            }
            var db = new DAL.dbDataContext();
            if (db.Products.Where(x => x.ID != product.ID && x.Name.Trim() == txt_Name.Text.Trim()).Count() > 0)
            {
                txt_Name.ErrorText = "هذا الاسم مسجل بالفعل";
                return false;
            }
            if (db.Products.Where(x => x.ID != product.ID && x.Code.Trim() == txt_Code.Text.Trim()).Count() > 0)
            {
                txt_Code.ErrorText = "هذا الكود مسجل بالفعل";
                return false;
            }

            return true;
        }
        public override void Save()
        {
            if (ValdiateData() == false)
                return;
            var db = new DAL.dbDataContext();

            if (product.ID == 0)
                db.Products.InsertOnSubmit(product);
            else
                db.Products.Attach(product);

            SetData();
            db.SubmitChanges();
            var data = gridView1.DataSource as BindingList<DAL.ProductUnit >;
            foreach (var item in data)
            {
                item.ProductID = product.ID;
                if (string.IsNullOrEmpty(item.Barcode) )
                    item.Barcode = "";
            }
            sdb.SubmitChanges();
            base.Save();
        }
        Byte[] GetByteFromImage(Image image)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                try
                {
                    image.Save(stream, ImageFormat.Jpeg);
                    return stream.ToArray();
                }catch 
                {
                    return stream.ToArray();
                }
               
            }

        }
        public override void RefreshData()
        {
            //var db = new DAL.dbDataContext();
            //lkp_Category.Properties.DataSource = db.ProductCategories;
            using (var db = new DAL.dbDataContext())
            {
                lkp_Category.Properties.DataSource = db.ProductCategories
                    .Where(x => db.ProductCategories.Where(w => w.ParentID == x.ID).Count() == 0).ToList();
                repoUOM.DataSource = db.UnitNames.ToList();
            }
            base.RefreshData();
        }
        DAL.ProductUnit ins = new DAL.ProductUnit();
        private void frm_Product_Load(object sender, EventArgs e)
        {
            lkp_Category.Properties.DisplayMember = "Name";
            lkp_Category.Properties.ValueMember = "ID";
            lkp_Category.ProcessNewValue += Lkp_Category_ProcessNewValue;
            lkp_Category.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;

            lkp_Type.Properties.DataSource = new List<ValueAndID>() { 
                new ValueAndID() { ID = 0, Name  ="مخزني" },
                new ValueAndID() { ID = 1, Name  ="خدمي" }
            };
            lkp_Type.Properties.DisplayMember = "Name";
            lkp_Type.Properties.ValueMember = "ID";

            gridView1.OptionsView.ShowGroupPanel = false;
            gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
             
            gridView1.Columns[nameof(ins.ID)].Visible = false;
            gridView1.Columns[nameof(ins.ProductID)].Visible = false;
            RepositoryItemCalcEdit calcEdit = new RepositoryItemCalcEdit();
           
            gridControl1.RepositoryItems.Add(calcEdit);
            gridControl1.RepositoryItems.Add(repoUOM);

            gridView1.Columns[nameof(ins.SellPrice)].ColumnEdit = calcEdit;
            gridView1.Columns[nameof(ins.BuyPrice)].ColumnEdit = calcEdit;
            gridView1.Columns[nameof(ins.SellDiscount)].ColumnEdit = calcEdit;
            gridView1.Columns[nameof(ins.Factor)].ColumnEdit = calcEdit;
            gridView1.Columns[nameof(ins.UnitID)].ColumnEdit = repoUOM;


            repoUOM.ValueMember = "ID";
            repoUOM.DisplayMember = "Name";
            repoUOM.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;

            repoUOM.ProcessNewValue += RepoUOM_ProcessNewValue;

            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            gridView1.FocusedRowChanged += GridView1_FocusedRowChanged;

        }

        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            
            gridView1.Columns[nameof(ins.Factor)].OptionsColumn.AllowEdit = !(e.FocusedRowHandle == 0);
            
        }

        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        { 
            var row = e.Row as DAL.ProductUnit ;
            var view = sender as GridView;
            if (row == null)
                return;
            if(row.Factor <= 1 && e.RowHandle != 0)
            {
                e.Valid = false;
                view.SetColumnError(view.Columns[nameof(row.Factor)], "يجب ان تكون القيمه اكبر من 1 ");
            }
            if (row.UnitID  <= 0 )
            {
                e.Valid = false;
                view.SetColumnError(view.Columns[nameof(row.UnitID)], ErrorText);
            }


        }

        private void RepoUOM_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            if(e.DisplayValue is string value&& value.Trim () != string.Empty)
            {
                var NewObject = new DAL.UnitName() { Name = value.Trim() };
                using (DAL.dbDataContext db = new DAL.dbDataContext())
                {
                    db.UnitNames.InsertOnSubmit(NewObject);
                    db.SubmitChanges();
                }
               ((List<DAL.UnitName>)repoUOM.DataSource).Add(NewObject);
                e.Handled = true;

            }
        }

        public  enum  ProductType {
            Inventory,
            Service 
        }
        class ValueAndID
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        private void Lkp_Category_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            if(e.DisplayValue is string st&&st.Trim() != string.Empty)
            {
                var newObject =new DAL.ProductCategory() {  Name = st, ParentID = 0 , Number = "0"};
                using (var db = new DAL.dbDataContext())
                {
                    db.ProductCategories.InsertOnSubmit(newObject);
                    db.SubmitChanges();
                }
               ((List<DAL.ProductCategory>)lkp_Category.Properties.DataSource).Add(newObject);
                e.Handled = true;
          
            }
        }
    }
}


